module.exports = {
    // configuration
	context: __dirname,
	devtool: "source-map",
    entry: "./scripts/main",
    module: {
        loaders: [
            { 
                test: /\.css$/, 
                loader: "style-loader!css-loader?importLoaders=1" 
            },
            { 
                test: /\.(png|woff|woff2|eot|ttf|svg)$/, 
                loader: 'url-loader?limit=100000' 
            }
        ]
    },
    output: {
        path: __dirname,
        filename: "index.js",
		sourceMapFilename: "./scripts/[file].map"
    },
	keepalive: false,
	watch: true
};
