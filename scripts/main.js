/* Practicing AngularJS modules, controllers and directives:
 * https://github.com/Revlin/practice-ng
 * 
 * Build with:
 * $ webpack --watch
 * or:
 * $ grunt webpack
 */
'use strict';

try {
var Debugger = require("./Debugger"),
    Configure = require("./services/Configure"),
    MainCtrl = require("./controllers/MainCtrl"),
    FormCtrl = require("./controllers/FormCtrl"),
	practice = require("./practice-common");
	
	require("../libs/jquery/dist/jquery");
	require("../libs/AngularJS/dist/angular");
	require("../libs/AngularJS/dist/angular-route");
	require("../libs/ngModal/dist/ng-modal");
    
    require("../libs/bootstrap/dist/css/bootstrap.min.css");
    require("../libs/bootstrap/dist/css/bootstrap-theme.min.css");
    require("../libs/ngModal/dist/ng-modal.css");
    
    require("../styles/main.css");
} catch(e) {} finally { 1; }

var main = angular.module('practice-angularjs', [ 
    'practice-common', 
    'ngModal',
    'ngRoute' 
]);

main.config([
	'$locationProvider',
	'$routeProvider',
	Configure(true) // initialize = true
]);

main.controller('mainController', [
    '$scope',
    'config',
    MainCtrl
]);

main.controller('formController', [
	'$scope', 
	'$http',
	'$location',
	'Users',
	FormCtrl
]);

main.service('config', Configure());


/* Adapted from "Learning AngularJS"
 * by Ken Williamson (O'Reilly, 2015)
 */
main.controller('userController', [
	'$scope',
	'$routeParams',
    'config',
	'Users',
	function( $scope, $routeParams, config, Users ) {
        Debugger.on = true; 
        config.dialogTitle = "Edit User";
        
	try {
		var userId = $routeParams.id || 1;
		
		$scope.user = {};
		
		if( $routeParams.name || $routeParams.email ) {
			$scope.user = Users.addUser({
				"id": $routeParams.id || 0,
				"name": $routeParams.name || "No User",
				"email": $routeParams.email || "No Email"
			});
		} else {
			Users.getById(userId).then(function( user ) {
				Debugger.log( user );

				if( user ) {
					$scope.user = user;
				} else {
					$scope.user = Users.addUser({
						"id": $routeParams.id || 0,
						"name": $routeParams.name || "No User",
						"email": $routeParams.email || "No Email"
					});
				}

				$scope.user.change = 
				function( uName, uMail ) {
					$scope.user.name = (uName)? uName : $scope.user.name;
					$scope.user.email = (uMail)? uMail : $scope.user.email;
				};
			});
		}
	
	} catch(e) {
		Debugger.log( e.stack );
	}
	}
]);
main.controller('addUserController', [
	'$scope',
	'$location',
    'config',
	function( $scope, $location, config ) {
        config.dialogTitle = "Add User";
        
		$scope.submit = 
		function() {
			$location.path('/user/'+ $scope.name +'/'+ $scope.email);
		};
	}
]);
