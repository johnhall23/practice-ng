/* Practicing AngularJS modules, controllers and directives:
 * https://bitbucket.org/real-currents/practice-ng
 */
'use strict';
try {
var Debugger = require("../Debugger"),
    configuration = require("../../config");
} catch(e) {} finally { 1; }

function Configure( initialize ) {
    Debugger.log( "Initialize configuration: "+ initialize );
    
    if( initialize ) return function( $locationProvider, $routeProvider ) {
        $locationProvider.html5Mode(false).hashPrefix('!');

        for( var route in configuration.dialog.routes )
            $routeProvider.when(route, {
                templateUrl: configuration.dialog.routes[route].templateUrl,
                controller: configuration.dialog.routes[route].controller
            });
        
        return true;
    }; 
    else return function() {
        var config = {
            get title() {
                return configuration.title;
            },
            get dialogTitle() {
                return configuration.dialog.title;
            },
            set dialogTitle( text ) {
                configuration.dialog.title = text;
                return configuration.dialog.title;
            }
        };
        
        return config; 
    };
};

try {
module.exports = Configure;
} catch(e) {} finally { 1; }
