/* Practicing AngularJS modules, controllers and directives:
 * https://bitbucket.org/real-currents/practice-ng
 */
'use strict';
try {
var Debugger = require("../Debugger");
} catch(e) {} finally { 1; }

function MainCtrl( $scope, config ) {
    Debugger.log( config ); 
    
    $scope.config = config;
}

try {
module.exports = MainCtrl;
} catch(e) {} finally { 1; }
