/* Adapted from "Submitting Ajax Forms: The AngularJS Way" 
 * by Chris Sevilleja
 * https://scotch.io/tutorials/submitting-ajax-forms-the-angularjs-way
 *
 * ... here's the AngularJS form controller, also using validation ala
 * https://scotch.io/tutorials/angularjs-form-validation
 */
'use strict';
try {
var Debugger = require("../Debugger");
} catch(e) {} finally { 1; }

function FormCtrl( $scope, $http, $location, Users ) {
    Debugger.on = true;

    $scope.formData = {};
    $scope.formErrors = {};		
    $scope.formSubmit = function( userID, isValid ) {
        
        if (userID) $scope.formData.uid = $scope.formData.uid || userID;
        
        if(! isValid ) {
            if( window.responseMessage )
                window.responseMessage.innerHTML = '<p class="warning">Form Data Is Not Valid!</p>'
        } else { 
            Debugger.log( $scope.formData );
            
            $http({
                method: 'GET',
                url: '/process',
                params: $scope.formData,
                headers: { 'Content-type': 'application/x-www-form-urlencoded' }
            })
                .success(function( data ) {

                    if( data.message ) {
                        if( window.responseMessage )
                             window.responseMessage.innerHTML = data.message;	
                    }

                    if( data.success ) {
                        $scope.formErrors = {};
                        $location.path('/user/'+ userID +'/'+ data.name +'/'+ data.email);

                    } else if( data.errors ) {
                        $scope.formErrors = data.errors;
                    }

                    return true;
                });

            return true;
        }
    };
}

try {
module.exports = FormCtrl;
} catch(e) {} finally { 1; }
