/* Practicing AngularJS modules, controllers and directives:
 * https://github.com/Revlin/practice-ng
 */
'use strict';
Debugger.on = true;

var practice = angular.module('practice-angularjs', [ 'practice-common' ]);


practice.config([
	'$locationProvider',
	function( $locationProvider ) {
		$locationProvider.html5Mode(false).hashPrefix('!');
    }
]);


practice.controller( 'HelloController', function($scope) {
try {
	"debugger";
	$scope.greeting = "Hello, $1!";
	
	$scope.greet = function(n) {
		return $scope.greeting.replace(/\$1/, n);
	};
	
	Debugger.log( this );
} catch(e) {
	Debugger.log( e.stack );
}
} );


/* Adapted from "Submitting Ajax Forms: The AngularJS Way" 
 * by Chris Sevilleja
 * https://scotch.io/tutorials/submitting-ajax-forms-the-angularjs-way
 *
 * ... here's the AngularJS form controller, also using validation ala
 * https://scotch.io/tutorials/angularjs-form-validation
 */
practice.controller('formController', [
	'$scope', 
	'$http',
	'$location',
	'Users',
	function( $scope, $http, $location, Users ) {
		
		$scope.formData = {};
		$scope.formErrors = {};		
		$scope.formSubmit = function( userID, isValid ) {
			if(! isValid ) {
				window.responseMessage.innerHTML = '<p class="warning">Form Data Is Not Valid!</p>'
			} else { 
				$http({
					method: 'GET',
					url: '/process',
					params: $scope.formData,
					headers: { 'Content-type': 'application/x-www-form-urlencoded' }
				})
					.success(function( data ) {

						if( data.message ) {
							window.responseMessage.innerHTML = data.message;	
						}

						if( data.success ) {
							$scope.formErrors = {};
							$location.path('/user/'+ userID +'/'+ data.Name +'/'+ data.Email);

						} else if( data.errors ) {
							$scope.formErrors = data.errors;
						}

						return true;
					});

				return true;
			}
		};
	}
]);


/* Adapted from "Learning AngularJS"
 * by Ken Williamson (O'Reilly, 2015)
 */
practice.controller('userController', [
	'$scope',
	'$location',
	'Users',
	function( $scope, $location, Users ) {
        var userId = 1;
		$scope.user = {};
        
	try {        
        $scope.params = $location.path().match(/\/user\/(\d+)\/?(\w+)?\/?([\w|\-|\_|\@|\.]+)?/);
        
        if( $scope.params !== null ) userId = $scope.params[1];

		if( $scope.params !== null && ($scope.params[2] || $scope.params[3]) ) {
			$scope.user = Users.addUser({
				"id": $scope.params[1] || 0,
				"name": $scope.params[2] || "No User",
				"email": $scope.params[3] || "No Email"
			});
		} else {
			Users.getById(userId).then(function( user ) {
				Debugger.log( user );

				if( user ) {
					$scope.user = user;
				} else {
					$scope.user = Users.addUser({
						"id": $scope.params[1] || 0,
				        "name": $scope.params[2] || "No User",
				        "email": $scope.params[3] || "No Email"
					});
				}

				$scope.user.change = 
				function( uName, uMail ) {
					$scope.user.name = (uName)? uName : $scope.user.name;
					$scope.user.email = (uMail)? uMail : $scope.user.email;
				};
			});
		}
	
	} catch(e) {
		Debugger.log( e.stack );
	}
	}
]);
practice.controller('addUserController', [
	'$scope',
	'$location',
	function( $scope, $location ) {
		$scope.submit = 
		function() {
			$location.path('/user/'+ $scope.name +'/'+ $scope.email);
		};
	}
]);

1;
