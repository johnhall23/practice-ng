/* Adapted from http://jsfiddle.net/eitanp461/qTvMz/
 */
'use strict';

//--- CODE --------------------------
var myProviders = angular.module('myProviders', []);

// Create a map-based configuration service 
myProviders.provider('MyService', function () {
    var service = { "about": "I am a service." };
    service.configureSomething = function () {
        return 461;
    };
    service.$get = function () {
        return {};
    };
    return service;
});

//--- SPEC --------------------------
describe("myProviders\n", function () {
    var myServiceProvider;
    
    beforeEach(function () {
        // Initialize the service provider by injecting it to a fake module's config block
        angular.module('testApp', [])
            .config(function (MyServiceProvider) {
                myServiceProvider = MyServiceProvider;
            });
        // Initialize myProviders injector
        module('myProviders', 'testApp');

        // Kickstart the injectors previously registered with calls to angular.mock.module
        inject(function () {});
    });

    it("will initialize as an angular service provider\n\n", function () {
        expect(myServiceProvider).toBeDefined();
    });

    it("will have a $get method\n\n", function () {
        expect(myServiceProvider.$get).toBeDefined();
        expect(typeof myServiceProvider.$get).toBe('function');
    });

    it("will have a configureSomething method\n\n", function () {
        expect(myServiceProvider.configureSomething).toBeDefined();
        expect(typeof myServiceProvider.configureSomething).toBe('function');
        expect(myServiceProvider.configureSomething()).toEqual(461);
    });
	
});
