/* Adapted from "Learning AngularJS"
 * by Ken Williamson (O'Reilly, 2015)
 *
 * Test userController
 */

describe( "useController...\n", function () {
	var scope, ctrl;
	
	beforeEach(function () { Debugger.on = true; });
    
	beforeEach(module('practice-angularjs', 'practice-common'));
    
	beforeEach(inject(
		function ($controller, $rootScope) {
			scope = $rootScope.$new();
			ctrl = $controller('userController', { 
				$scope: scope, 
				$routeParams:  {}
			});
		}
	));
	
	it("should be available\n\n", function() {
		expect(ctrl).toBeDefined();
	});
	
	/* Need to implement asynchronous testing from this point on */
	
	xit("should define a user\n\n", function () {
		expect(scope.user).toEqual(jasmine.any(Object));
	});
	
	xit("should define a user id\n\n", function () {
		expect(scope.user.id).toEqual(jasmine.any(Number));
	});
	
	xit("should define a user name\n\n", function () {
		expect(scope.user.name).toEqual(jasmine.any(String));
	});
	
	xit("should define a user email\n\n", function () {
		expect(scope.user.email).toEqual(jasmine.any(String));
	});
	
	xit("should have method to change user\n\n", function () {
		expect(scope.changeUser).toEqual(jasmine.any(Function));
	});
	
	xit("should change user name\n\n", function () {
		Debugger.log( "User name: "+ scope.user.name );
		scope.uName = "New Name";
		scope.changeUser();
		var name = Debugger.log( scope.user.name, "User name: $1" );
		expect(name).toEqual("New Name");
	});

	afterEach(function () { Debugger.on = false; });
			   
} );

1;
