#!/usr/bin/env perl
use 5.10.1;
use strict;
use warnings;
use Cwd qw(chdir cwd getcwd);
use Fcntl (':flock', 'O_RDWR', 'SEEK_END');
use Mojolicious::Lite;
use Mojo::JSON ('decode_json', 'encode_json', 'false');
use Mojo::Log;
use Tie::File;

my $fh;
my $errors = {};
my $log = Mojo::Log->new; 

our $version = Mojolicious->VERSION;

# Add current working directory as path to static files
push @{app->static->paths}, getcwd();

sub writeData;

get('/' => sub {
  	my $c = shift;
	$c->reply->static('index.html');
});

get('/process' => sub {
	my $c = shift;
	my $msg = "Form Recieved";
	my $data = {};
	
	unless (($data->{'name'}) = $c->param('uname') =~ /([\.|\w|\-|\_|\s]+)/) {
	 	 $errors->{'name'} = "Name is required.\n";
		 $data->{'name'} = "!";
	}
	
	unless (($data->{'email'}) = $c->param('umail') =~ /([\.|\w|\-|\_|\s]+\@[\.|\w|\-|\_|\s]+)/) {
		$errors->{'email'} = "Email is required.\n";
		$data->{'email'} = "!";
	}
	
	unless (($data->{'id'}) = $c->param('uid') =~ /(\d+)/ && ($data->{'id'} >= 1)) {
		 $log->debug("Add new user");
	}
	
	unless (keys %$errors) {
        foreach my $item (keys %$data) {
            $msg .= "<br /><br />\n ". $item .": ". $data->{$item};
        }
		$data->{'success'} = 'true';
		$data->{'message'} = $msg;
        
        writeData ($data);
        
	} else {
        foreach my $error (keys %$errors) {
            $msg .= "<br /><br />\n Error: ". $errors->{$error};
        }
		$data->{'success'} = false;
		$data->{'errors'} = $errors;
		$data->{'message'} = $msg;		
	}	
	
	$c->render(json => $data);
});

sub writeData {
    my $data = shift;
    my @uids = ();
    my @user_list;
    
    $log->debug("Update user ". $data->{'id'}) if ($data->{'id'});
    
    open $fh, '+<', 'data/users.json' 
        or $errors->{'users'} = "Could not open user list: $!";
    flock($fh, LOCK_EX) 
        or $errors->{'users'} = "Could not lock filehandle to contact list: $!";
    tie @user_list, 'Tie::File', $fh;
    
    $log->debug( "User line count: ". $#user_list );
    
    my $idx = 0;
    for my $user (@user_list) {
        $log->debug( "$idx: $user" );
        
        unless ( $user =~ /\[$/ ) {
#            unless ($data->{'id'} && ($data->{'id'} >= 1)) {
#                # Add new user
#                if( $user =~ /^\]/ ) {
#                    $user_list[($idx - 1)] = $user_list[($idx - 1)] .","
#                        unless( $user_list[($idx - 1)] =~ /\[/ );
#                    $user_list[$idx] = "\t". &encode_json($saved) ."\n]";
#                    $log->debug( encode_json($saved) );
#                }
#            } else {
#                # Update user
#            }
            $idx++;
        }
    }
    
    flock($fh, LOCK_UN) or $errors->{'users'} = "Could not unlock filehandle to contact list: $!";
    close $fh or $errors->{'users'} = "Could not close contact list: $!";

    if(keys %$errors) {
        $log->debug("Error: ". $errors->{$_}) for (keys %$errors);
        return 0;
    }
    
    return 1;
}

unless( -e -r -w 'data/users.json' ) {
    # Initialize user list if none exists
    open $fh, '>', 'data/users.json' 
        or $errors->{'users'} = "Could not create contact list: $!";
    $fh->print("[\n]\n");
    close $fh or $errors->{'users'} = "Could not close contact list: $!";
}

$log->debug("Error: ". $errors->{$_}) for (keys %$errors);

app->start;
