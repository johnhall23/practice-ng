Practice AngularJS
==================

####Learning AngularJS design and technique across multiple stacks:

	1) Mojolicious (Node-like)

	2) HTTP Server with CGI support (LAMP-like)

	3) IIS with ASP.NET (WISA)
...

####Querying multiple databases:

	1) JSON (flat-file)

	2) MongoDB (NoSQL)

	3) PostgreSQL (SQL)
...

Before running ```npm install```, please install bower, karma (Node) and cpanminus (Perl) globally with these respective commands:
```
$ sudo npm -g install bower karma karma-cli karma-chrome-launcher karma-firefox-launcher karma-jasmine
```
```
$ sudo cpan App::cpanminus
```

These demos make use of webpack to assemble the various JavaScript files into a single source file.  After installing the local npm dependencies, build 'index.js' with:
```
$ webpack --watch
```
or
```
$ grunt webpack
```


####AngularJS runs on service objects and special purpose objects. 

#####Services:

	Constant
	Provider
	
	Value
	Factory
	Service


While *Constant* and *Provider* services can be accessed during the configuration phase of an Angular app, *Value*, *Factory* and *Service* services become available at run-time.

#####Special Purpose Objects:

	Controller
	Directive
	Filter
	Animation

