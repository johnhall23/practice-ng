function Grunt( grunt ) {
	var webpack = require("webpack");
	var webpackConfig = require("./webpack.config");
	
	require("matchdep").filterAll("grunt-*").forEach(grunt.loadNpmTasks);
	
	grunt.initConfig({
		bgShell: {
			_defaults: {
				bg: true
			},
			karma: {
				cmd: 'karma start'
			},
			server: {
				cmd: 'morbo practice-server.pl'
			}
		},
		webpack: {
			options: webpackConfig,
			devBuild: {
				devtool: "sourcemap",
				debug: true
			},
			proBuild: {
//				plugins: webpackConfig.plugins.concat(
//					new webpack.DefinePlugin({
//						"process.env": {
//							// This has effect on the react lib size
//							"NODE_ENV": JSON.stringify("production")
//						}
//					}),
//					new webpack.optimize.DedupePlugin(),
//					new webpack.optimize.UglifyJsPlugin()
//				)
			}
		}
	});
    
    grunt.config('watch', {
        "scripts": {
			files: [
				"./scripts/*.js", 
				"./scripts/**/*.js",
				"./styles/*.css"
			],
			tasks: [ "webpack:devBuild" ],
			options: {
				spawn: false,
			}
        }
    });

	// Development build
	grunt.registerTask("dev", [ "webpack:devBuild" ]);

	// Production build
	grunt.registerTask("build", [ "webpack:proBuild" ]);

	// Serve with Mojolicious
	grunt.registerTask("serve", [ "bgShell:server" ]);

	// Test with Karma
	grunt.registerTask("test", [ "bgShell:karma", "watch" ]);
    
    // Default tasks
	grunt.registerTask("default", [ "dev", "serve", "watch" ]);
};

module.exports = Grunt;
